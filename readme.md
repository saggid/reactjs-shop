## React приложение магазина

Webpack-конфиги по большей части взяты с [этого руководства](https://maxfarseer.gitbooks.io/react-router-course-ru/content/nastraivaem_dev-okruzhenie.html).

Код написан в стиле ES2015 с использованием Webpack и Babel.

### Используемые библиотеки

- [React](https://facebook.github.io/react/docs/)
- [Reflux.js](https://github.com/reflux/refluxjs) (как предлагали в ТЗ, взял её)
- [Sugar.js](http://sugarjs.com/) (люблю эту маленькую штучку)

### Установка и запуск:

```bash
npm install
npm start
```

### Сборка для продакшена:
```bash
webpack --config webpack/common.config
```
