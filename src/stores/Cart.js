import Reflux from 'reflux'
import Actions from 'actions'

export default Reflux.createStore({

    init: function() {
        this.count = 0
        this.items = []
        this.listenTo(Actions.AddedItemToCart, this.onAddedItemToCart)
    },

    onAddedItemToCart: function(item) {
      this.count += 1
      this.items.push(item)
      this.trigger(this.count);
    }

});
