import Reflux from 'reflux'
//import Actions from 'actions'

export default Reflux.createStore({

    // Initial setup
    init: function() {
        this.items = [
          {
            id: 1,
            img: 'http://lorempixel.com/240/165/technics?1',
            title: 'Проектор DL-20',
            desc: 'Cтационарный широкоформатный проектор. Технология SXRD, поддержка 3D и HDTV, разрешение 1920x1080',
            price: '559 900 руб.',
            category_id: 1
          },
          {
            id: 2,
            img: 'http://lorempixel.com/240/165/technics?2',
            title: 'Проектор ПФ-50',
            desc: 'Очень мощный проектор.',
            price: '280 000 руб.',
            category_id: 1
          },
          {
            id: 3,
            img: 'http://lorempixel.com/240/165/technics?3',
            title: 'Экран Apple S20',
            desc: 'Новое слово в индустрии экранов.',
            price: '54 000 руб.',
            category_id: 2
          },
          {
            id: 4,
            img: 'http://lorempixel.com/240/165/technics?4',
            title: 'Звуковая панель D30',
            desc: 'Новое слово в индустрии звука.',
            price: '22 000 руб.',
            category_id: 3
          },
          {
            id: 5,
            img: 'http://lorempixel.com/240/165/technics?5',
            title: 'Звуковая панель D20',
            desc: 'Новое слово в индустрии звука.',
            price: '23 000 руб.',
            category_id: 3
          }
        ]
    }

});
