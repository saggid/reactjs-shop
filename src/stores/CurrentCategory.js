import Reflux from 'reflux'
import Actions from 'actions'

export default Reflux.createStore({

    init: function() {
        this.category_id = 1
        this.listenTo(Actions.SelectedCategory, this.onSelectedCategory)
    },

    onSelectedCategory: function(category_id) {
      this.category_id = category_id
      this.trigger(category_id);
    }

});
