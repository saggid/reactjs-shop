import Reflux from 'reflux'

export default Reflux.createActions([
    'AddedItemToCart',
    'SelectedCategory'
]);
