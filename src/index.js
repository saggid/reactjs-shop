import 'babel-polyfill'
import 'sugar'
import React from 'react'
import { render } from 'react-dom'

// Стили
import './static/css/style.css'

import App from './containers/App'

render(
  <App />,
  document.getElementById('root')
)
