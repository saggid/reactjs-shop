$(function () {
  'use strict';
  // functions
  function initSelect() {
    $('select').styler();
  }

  function animateSearchForm() {
    $('.js-searchFormInput').focus(function () {
      $('body').addClass('blackout');
      $(this).parent().addClass('searchForm_status_focus'); // add class to .searchForm
    });
  }

  function showAutocomplite() {
    $('.js-searchFormInput').keypress(function () {
      $('.searchForm__list').addClass('searchForm__list_state_show');
    });
  }

  function blackout() {
    $('body').click(function (event) {
      if ($('body').hasClass('blackout') && event.target === this) {
        $(this).removeClass('blackout');

        $('.searchForm__list').removeClass('searchForm__list_state_show');
      }
    });
  }

  function svg() {
    new svgIcon(document.querySelector('.si-icon-hamburgerCross'), svgIconConfig, {easing: mina.backin});
  }

  function showNav() {
    $('[data-block="navBtn"]').on('click', function () {
      $('[data-block="nav"]').toggleClass('_show');
      $('body').toggleClass('_move');
      $('.header').toggleClass('_move');
    });
    //$('[data-block="nav"]')
  }

  function filter() {
    $('[data-filter="selectSection"]').find('.filter__productItem').on('click', function (e) {
      e.preventDefault();
      $('.filter__productItem').removeClass('filter__productItem_background_blue'); // remove all bg
      $(this).addClass('filter__productItem_background_blue'); // add this element bg
      $('.filter__allOptionsList').removeClass('_active');
      $('.filter__selectOptions').find($(this).attr('href')).addClass('_active');
    });
  }

  function initSlider() {
    $('[data-slider="image"]').bxSlider({
      pagerCustom: '.product__preview',
      nextText: '',
      prevText: ''
    });
  }


  // functions init
  initSelect();
  animateSearchForm();
  blackout();
  showAutocomplite();
  svg();
  showNav();
  filter();
  initSlider();
});