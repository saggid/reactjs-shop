import React from 'react'
import Cart from 'stores/Cart'

export default class Header extends React.Component {

  componentDidMount() {
    Cart.listen((state) => { this.setState({itemsInCart: state}) })
  }

  state = {
    itemsInCart: Cart.count
  }

  render() {
    return (
      <div className="header">
        <div className="header__logo">
          <div className="logo"></div>
        </div>
        <div className="header__navSite">
          <div data-block="navBtn" className="navSiteBtn"><span data-icon-name="hamburgerCross" className="si-icon si-icon-hamburgerCross"></span></div>
          <div data-block="nav" className="navSite">
            <div className="navSite__item"><a href="" className="navSite__link">Каталог</a></div>
            <div className="navSite__item"><a href="" className="navSite__link">Статьи</a></div>
            <div className="navSite__item"><a href="" className="navSite__link">Инструменты</a></div>
            <div className="navSite__item"><a href="" className="navSite__link">Контакты</a></div>
          </div>
        </div>
        <div className="header__basket">
          <a href="" className="basket">
          <span className="basket__icon i-basket"></span>
          <div className="basket__text">Корзина ({this.state.itemsInCart})</div></a>
        </div>
      </div>
    )
  }
}
