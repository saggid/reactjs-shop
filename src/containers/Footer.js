import React, { Component } from 'react'

export default class Footer extends Component {
  render() {
    return (
      <div className="footer">
        <div className="footer__social">
          <div className="social">
            <div className="social__item"><a href="" className="social__facebook i-fb"></a></div>
            <div className="social__item"><a href="" className="social__twitter i-tw"></a></div>
          </div>
        </div>
      </div>
    )
  }
}
