import React, { Component } from 'react'

import SearchForm      from './main/SearchForm'
import FilterPanel     from './main/FilterPanel'
import ItemsList       from './main/ItemsList'
import Categories      from 'stores/Categories'
import CurrentCategory from 'stores/CurrentCategory'

export default class Main extends Component {

  componentDidMount() {
    CurrentCategory.listen(() => { this.forceUpdate() })
  }

  render() {

    let cat = Categories.items.find({ id : CurrentCategory.category_id })

    return (
      <div className="main">
        <SearchForm />
        <FilterPanel />

        <div className="content content_width_70">
          <h1>{cat.title}</h1>
          <p>{cat.desc}</p>
        </div>

        <ItemsList />

    </div>
  )
  }
}
