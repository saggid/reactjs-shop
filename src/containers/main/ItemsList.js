import React, { Component } from 'react'
import Cart from 'stores/Cart'
import Items from 'stores/Items'
import Actions from 'actions'
import CurrentCategory from 'stores/CurrentCategory'

export default class ItemsList extends Component {

  componentDidMount() {
    Cart.listen(() => { this.forceUpdate() })
    CurrentCategory.listen(() => { this.forceUpdate() })
  }

  /**
   * Клик по кнопке "добавить в Корзину"
   **/
  addToCart(item, event) {
    event.preventDefault()
    Actions.AddedItemToCart(item)
  }

  render() {
    return (
      <div className="list list_type_product">
      { Items.items.findAll({category_id : CurrentCategory.category_id}).map((item) => {
          let countItemsInCart = Cart.items.count({id: item.id})
          let counteItemsInCartText = countItemsInCart ? ` (${countItemsInCart})` : ''
          return (
            <div key={item.id} className="list__item list__item_size_wide">
              <a href="" className="productShort productShort_size_wide">
                <div className="productShort__card">
                  <div className="productShort__img productShort__img_height_high">
                    <img src={item.img} alt=""/>
                  </div>
                  <div className="productShort__title">{item.title}</div>
                  <div className="productShort__desc productShort__desc_height_short">{item.desc}</div>
                  <div className="productShort__price">{item.price}</div>
                  <div className="productShort__btn">
                    <button className="btn btn_color_blue" onClick={this.addToCart.bind(this,item)} >В корзину{counteItemsInCartText}</button>
                  </div>
                </div>
              </a>
            </div>
          )
      })}
      </div>
    )
  }
}
