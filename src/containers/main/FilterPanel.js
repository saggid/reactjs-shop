import React, { Component } from 'react'
import Categories from 'stores/Categories'
import Actions from 'actions'

export default class FilterPanel extends Component {

  state = {
    category: 1 // Текущая активная категория
  }

  // Обработка события выбора одной из категорий
  selectCategory(item, event) {
    this.setState({ category: item.id })
    Actions.SelectedCategory(item.id)
    event.preventDefault()
  }

  render() {
    return (
      <div>
        <div className="filter">
          <div data-filter="selectSection" className="filter__selectProduct">
            { Categories.items.map((item) => {
                let classes = 'filter__productItem'
                classes += this.state.category === item.id ? ' filter__productItem_background_blue' : ''
                return (
                  <a key={item.id} href="#" className={classes} onClick={this.selectCategory.bind(this,item)}>
                    <div className={`filter__productIcon i-${item.class}`}></div>
                    <div className="filter__productName">{item.title}</div>
                  </a>
                )
            })}
          </div>

          <div className="filter__selectOptions">
            <div id="project" className="filter__allOptionsList _active">
              <div className="filter__column">
                <div className="filter__optionsTitle">Комплекты</div>
                <div className="filter__optionsList">
                  <div className="filter__option filter__option_selected_true">Для дома</div>
                  <div className="filter__option">Для офиса</div>
                  <div className="filter__option">Для школы</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle">Тип</div>
                <div className="filter__optionsList">
                  <div className="filter__option">3D проекторы</div>
                  <div className="filter__option">LED проекторы</div>
                  <div className="filter__option">Видеопроектоы</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle">Бренд</div>
                <div className="filter__optionsList">
                  <div className="filter__option">Mitsubishi</div>
                  <div className="filter__option">Sony</div>
                  <div className="filter__option">Epson</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle"></div>
                <div className="filter__optionsList">
                  <div className="filter__option">Simens</div>
                  <div className="filter__option">Nec</div>
                </div>
              </div>
            </div>
            <div id="screen" className="filter__allOptionsList">
              <div className="filter__column">
                <div className="filter__optionsTitle">Комплекты</div>
                <div className="filter__optionsList">
                  <div className="filter__option">Для дома2</div>
                  <div className="filter__option">Для офиса</div>
                  <div className="filter__option">Для школы</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle">Тип</div>
                <div className="filter__optionsList">
                  <div className="filter__option">3D проекторы</div>
                  <div className="filter__option">LED проекторы</div>
                  <div className="filter__option">Видеопроектоы</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle">Бренд</div>
                <div className="filter__optionsList">
                  <div className="filter__option">Mitsubishi</div>
                  <div className="filter__option">Sony</div>
                  <div className="filter__option">Epson</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle"></div>
                <div className="filter__optionsList">
                  <div className="filter__option">Simens</div>
                  <div className="filter__option">Nec</div>
                </div>
              </div>
            </div>
            <div id="soundbar" className="filter__allOptionsList">
              <div className="filter__column">
                <div className="filter__optionsTitle">Комплекты</div>
                <div className="filter__optionsList">
                  <div className="filter__option">Для дома3</div>
                  <div className="filter__option">Для офиса</div>
                  <div className="filter__option">Для школы</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle">Тип</div>
                <div className="filter__optionsList">
                  <div className="filter__option">3D проекторы</div>
                  <div className="filter__option">LED проекторы</div>
                  <div className="filter__option">Видеопроектоы</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle">Бренд</div>
                <div className="filter__optionsList">
                  <div className="filter__option">Mitsubishi</div>
                  <div className="filter__option">Sony</div>
                  <div className="filter__option">Epson</div>
                </div>
              </div>
              <div className="filter__column">
                <div className="filter__optionsTitle"></div>
                <div className="filter__optionsList">
                  <div className="filter__option">Simens</div>
                  <div className="filter__option">Nec</div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="filter_mobile">
          <div className="filter_mobile__selectProduct">
            <div className="filter_mobile__productItem">
              <div className="filter_mobile__choiseProduct _active">
                <div className="filter_mobile__productIcon i-project"></div>
                <div className="filter_mobile__productName">Проекторы</div>
              </div>
              <div className="filter_mobile__allOptionsList _active">
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Комплекты</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option filter_mobile__option_selected_true">Для дома</div>
                    <div className="filter_mobile__option">Для офиса</div>
                    <div className="filter_mobile__option">Для школы</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Тип</div>
                  <div className="filter_mobile__optionsList _active">
                    <div className="filter_mobile__option">3D проекторы</div>
                    <div className="filter_mobile__option">LED проекторы</div>
                    <div className="filter_mobile__option">Видеопроектоы</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Бренд</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">Mitsubishi</div>
                    <div className="filter_mobile__option">Sony</div>
                    <div className="filter_mobile__option">Epson</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle"></div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">Simens</div>
                    <div className="filter_mobile__option">Nec</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="filter_mobile__productItem">
              <div className="filter_mobile__choiseProduct">
                <div className="filter_mobile__productIcon i-screen"></div>
                <div className="filter_mobile__productName">Экраны</div>
              </div>
              <div className="filter_mobile__allOptionsList">
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Комплекты</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option filter_mobile__option_selected_true">Для дома</div>
                    <div className="filter_mobile__option">Для офиса</div>
                    <div className="filter_mobile__option">Для школы</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Тип</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">3D проекторы</div>
                    <div className="filter_mobile__option">LED проекторы</div>
                    <div className="filter_mobile__option">Видеопроектоы</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Бренд</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">Mitsubishi</div>
                    <div className="filter_mobile__option">Sony</div>
                    <div className="filter_mobile__option">Epson</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle"></div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">Simens</div>
                    <div className="filter_mobile__option">Nec</div>
                  </div>
                </div>
              </div>
            </div>
            <div className="filter_mobile__productItem">
              <div className="filter_mobile__choiseProduct">
                <div className="filter_mobile__productIcon i-soundbar"></div>
                <div className="filter_mobile__productName">Звуковые панели</div>
              </div>
              <div className="filter_mobile__allOptionsList">
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Комплекты</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option filter_mobile__option_selected_true">Для дома</div>
                    <div className="filter_mobile__option">Для офиса</div>
                    <div className="filter_mobile__option">Для школы</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Тип</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">3D проекторы</div>
                    <div className="filter_mobile__option">LED проекторы</div>
                    <div className="filter_mobile__option">Видеопроектоы</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle">Бренд</div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">Mitsubishi</div>
                    <div className="filter_mobile__option">Sony</div>
                    <div className="filter_mobile__option">Epson</div>
                  </div>
                </div>
                <div className="filter_mobile__column">
                  <div className="filter_mobile__optionsTitle"></div>
                  <div className="filter_mobile__optionsList">
                    <div className="filter_mobile__option">Simens</div>
                    <div className="filter_mobile__option">Nec</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    )
  }
}
