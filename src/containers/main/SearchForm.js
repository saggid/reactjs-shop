import React, { Component } from 'react'

export default class SearchForm extends Component {
  render() {
    return (
      <div className="searchForm">
        <input type="text" className="searchForm__input input js-searchFormInput"/>
        <button className="searchForm__btn i-magnifier"></button>
        <div className="searchForm__list">
          <div className="searchForm__item">Sony VPL HW40ES</div>
          <div className="searchForm__item">Sony VPL VW300ES</div>
          <div className="searchForm__item">Sony VPL VW55ES</div>
        </div>
      </div>
    )
  }
}
